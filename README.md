# React Form App

## DB - mongo
#### to start mongodb
```bash
docker-compose up
```

## SERVER

### go to server
```bash
cd server
```

#### append demo-data to db *-start mongo
```bash
npm run demo
```

#### to start dev *-start mongo
```bash
npm run dev
```
#### to test *-stop dev server
```bash
npm run test
```

#### to test flow
```bash
npm run flow
```


## CLIENT

#### to start dev *-start mongo and dev-server
```bash
npm start
```
#### to start build
```bash
npm run build
```
#### to test
```bash
npm run test
```

#### to test flow
```bash
npm run flow
```

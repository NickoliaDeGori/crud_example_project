import assert from  'assert';
import cyrillicToTranslit from '../lib/utilits/cyrillicToTranslit';

describe('Cyrillic Test', function() {
  it('returns Маша=Masha', function(done) {
    assert.equal(cyrillicToTranslit("Маша"), "Masha");
    done();
  });
  it('returns Masha=Masha', function(done) {
    assert.equal(cyrillicToTranslit("Masha"), "Masha");
    done();
  });
});

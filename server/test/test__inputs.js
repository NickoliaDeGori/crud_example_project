import assert from  'assert';
import {testName, testPhone} from "../lib/utilits/inputs";


describe('Cyrillic Test', function() {
  it('Holaba name is correct', (done) => {
    assert.equal(testName("Holaba"), true);
    done();
  });

  it('H name is incorrect', (done) => {
    assert.equal(testName("H"), false);
    done();
  });

  it('+78912 phone is correct', (done) => {
    assert.equal(testPhone("+78912"), true);
    done();
  });

  it('+ phone is correct', (done) => {
    assert.equal(testPhone("+"), false);
    done();
  });
});

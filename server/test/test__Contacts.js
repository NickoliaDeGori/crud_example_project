process.env.MONGO_BASE = 'contact_test';

import ContactSchema from '../lib/database/scheme/Contact';

import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../lib/index';

chai.should();

chai.use(chaiHttp);

describe('Contacts', () => {
  beforeEach((done) => {
    ContactSchema.remove({}, (err) => {
      if (err !== null && err !== undefined) {
        console.error(err);
      }
      done();
    });
  });

  it('it should create new contact', (done) => {

    let contact = {
      first_name: "Bob",
      last_name: "Celso",
      phone: "+79118567231",
      address: "Bob Bobji",
      email: "bobcelso@gamel.ocm",
    };

    chai.request(server)
        .post('/api/v1/contacts/create')
        .send(contact)
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('code').eql(200);
          res.body.should.have.property('data');
          res.body.data.should.have.property('id');
          res.body.data.should.have.property('first_name').eql(contact.first_name);
          res.body.data.should.have.property('last_name').eql(contact.last_name);
          res.body.data.should.have.property('phone').eql(contact.phone);
          res.body.data.should.have.property('address').eql(contact.address);
          res.body.data.should.have.property('email').eql(contact.email);

          done();
        });
  });

  it('it should not create incorrect new contact', (done) => {

    let contact = {
      first_name: "",
      last_name: "",
      phone: "",
      address: "",
      email: "",
    };

    chai.request(server)
        .post('/api/v1/contacts/create')
        .send(contact)
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.have.property('code').eql(500);
          JSON.parse(res.body.data).should.have.property('code').eql(1502);
          done();
        });
  });

  it('it should get old contact', (done) => {
    let contact = {
      first_name: "Bob",
      last_name: "Celso",
      phone: "+79118567231",
      address: "Bob Bobji",
      email: "bobcelso@gamel.ocm",
    };

    chai.request(server)
        .post('/api/v1/contacts/create')
        .send(contact)
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.data.should.have.property('id');
          chai.request(server)
              .get('/api/v1/contacts/get/' + res.body.data.id)
              .set('Accept', 'application/json')
              .end((err, response) => {
                response.should.have.status(200);
                response.body.should.have.property('data');
                response.body.data.should.have.property('id');
                response.body.data.should.have.property('first_name');
                response.body.data.should.have.property('last_name');
                response.body.data.should.have.property('phone');
                response.body.data.should.have.property('address');
                response.body.data.should.have.property('email');

                done();
              });

        });
  });

  it('it should delete contact', (done) => {

    let contact = {
      first_name: "Bob",
      last_name: "Celso",
      phone: "+79118567231",
      address: "Bob Bobji",
      email: "bobcelso@gamel.ocm",
    };

    chai.request(server)
        .post('/api/v1/contacts/create')
        .send(contact)
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.data.should.have.property('id');
          chai.request(server)
              .delete('/api/v1/contacts/delete/' + res.body.data.id)
              .set('Accept', 'application/json')
              .end((err, res) => {
                res.should.have.status(200);
                done();
              });

        });
  });

  it('it should update contact', (done) => {

    let contact = {
      first_name: "Bob",
      last_name: "Celso",
      phone: "+79118567231",
      address: "Bob Bobji",
      email: "bobcelso@gamel.ocm",
    };

    let updatedContact = {
        ...contact,
      phone: "+791135457231",
      address: "Bob Dilan",
    };

    chai.request(server)
        .post('/api/v1/contacts/create')
        .send(contact)
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.data.should.have.property('id');
          chai.request(server)
              .post('/api/v1/contacts/update/' + res.body.data.id)
              .send(updatedContact)
              .set('Content-Type', 'application/json')
              .end((err, response) => {
                response.should.have.status(200);
                response.body.should.have.property('data');
                response.body.data.should.have.property('id');
                response.body.data.should.have.property('first_name').eql(updatedContact.first_name);
                response.body.data.should.have.property('last_name').eql(updatedContact.last_name);
                response.body.data.should.have.property('phone').eql(updatedContact.phone);
                response.body.data.should.have.property('address').eql(updatedContact.address);
                response.body.data.should.have.property('email').eql(updatedContact.email);
                done();
              });

        });
  });

  it('it should not update incorrect new contact', (done) => {

    let contact = {
      first_name: "Bob",
      last_name: "Celso",
      phone: "+79118567231",
      address: "Bob Bobji",
      email: "bobcelso@gamel.ocm",
    };

    let updatedContact = {
      ...contact,
      phone: "",
    };

    chai.request(server)
        .post('/api/v1/contacts/create')
        .send(contact)
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.data.should.have.property('id');
          chai.request(server)
              .post('/api/v1/contacts/update/' + res.body.data.id)
              .send(updatedContact)
              .set('Content-Type', 'application/json')
              .end((err, response) => {
                response.should.have.status(500);
                response.body.should.have.property('code').eql(500);
                JSON.parse(response.body.data).should.have.property('code').eql(1502);
                done();
              });
      });
  });

  it('it should GET all the contacts', (done) => {
    chai.request(server)
        .get('/api/v1/contacts/all')
        .set('Accept', 'application/json')
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
  });
});

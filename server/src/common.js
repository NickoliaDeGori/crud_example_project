import jwt from 'express-jwt';

const secretKey = 'MY_SECRET';

export const auth = jwt({
  secret: secretKey,
  userProperty: 'auth'
});

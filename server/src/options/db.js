export const mongoHost = process.env.MONGO_HOST || 'localhost';
export const mongoPort = process.env.MONGO_PORT || 27017;
export const mongoBase = process.env.MONGO_BASE || 'crud';

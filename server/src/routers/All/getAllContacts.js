/* @flow */
import * as interfaces from "../../types/interfaces"
import ContactSchema from "../../database/scheme/Contact"
import {DB_ERROR} from "../../constants/errors";

export default () => {
  return new Promise(async (resolve, reject) => {
    try {
      const contacts = await ContactSchema.find({}).exec();
      const responseContacts: interfaces.Contacts = {
        contacts,
        count: contacts.length
      };
      resolve(responseContacts);
    } catch (err) {
      const errorObj: interfaces.Failure = {
        code: DB_ERROR,
        message: `Что-то пошло не так: ${err.toString()}`
      };
      reject(errorObj)
    }
  });
}

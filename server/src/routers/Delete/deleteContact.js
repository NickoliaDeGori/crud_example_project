/* @flow */
import * as interfaces from "../../types/interfaces"
import ContactSchema from "../../database/scheme/Contact";
import {DB_ERROR} from "../../constants/errors";
import {DB_REMOVE_ITEM_SUCCESS} from "../../constants/success";

export default (vars: { [string]: any },) => {
  return new Promise(async (resolve, reject) => {
    try {
      await ContactSchema.findOneAndRemove({
        id: vars["contact_id"]
      }).exec();

      const success: interfaces.Success = {
        code: DB_REMOVE_ITEM_SUCCESS,
        message: `Контакт удален.`
      };
      resolve(success);
    } catch (err) {
      const errorObj: interfaces.Failure = {
        code: DB_ERROR,
        message: `Что-то пошло не так: ${err.toString()}`
      };
      reject(errorObj)
    }
  });
}

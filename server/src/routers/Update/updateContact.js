/* @flow */
import * as interfaces from "../../types/interfaces"
import ContactSchema from "../../database/scheme/Contact";
import {DB_ERROR, INPUT_ERROR} from "../../constants/errors";
import {testName, testPhone} from "../../utilits/inputs";

export default (vars: { [string]: any },
                dataContactCore: interfaces.ContactCore,) => {
  return new Promise(async (resolve, reject) => {
    if (!testName(dataContactCore.first_name) ||
        !testPhone(dataContactCore.phone)) {

      const errorObj: interfaces.Failure = {
        code: INPUT_ERROR,
        message: `Введенные данные некорректны`
      };
      reject(errorObj)
    }
    try {
      const responseContact: interfaces.Contact = await ContactSchema.findOneAndUpdate({
        id: vars["contact_id"]
      }, {$set: {...dataContactCore}}, {new: true, overwrite: true}).exec();

      resolve(responseContact);
    } catch (err) {
      const errorObj: interfaces.Failure = {
        code: DB_ERROR,
        message: `Что-то пошло не так: ${err.toString()}`
      };
      reject(errorObj)
    }
  });
}

/* @flow */
import type { $Request, $Response } from 'express';
import * as interfaces from "../types/interfaces"
import getAllContacts from "./All/getAllContacts"
import getContact from "./Get/getContact"
import updateContact from "./Update/updateContact"
import createContact from "./Create/createContact"
import deleteContact from "./Delete/deleteContact"

export const GetAllContactsHandler = async (req: $Request, res: $Response) => {
  try {
    const response = await getAllContacts(
    );
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send({code: 200, data: response});
  } catch (err) {
    res.status(500).send({code: 500, data: JSON.stringify(err)});
  }
};

export const GetContactHandler = async (req: $Request, res: $Response) => {
  let vars = {};
  vars["contact_id"] = req.params["contact_id"];
  try {
    const response = await getContact(
      vars,
    );
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send({code: 200, data: response});
  } catch (err) {
    res.status(500).send({code: 500, data: JSON.stringify(err)});
  }
};

export const UpdateContactHandler = async (req: $Request, res: $Response) => {
  const dataContactCore: interfaces.ContactCore = req.body;
  let vars = {};
  vars["contact_id"] = req.params["contact_id"];
  try {
    const response = await updateContact(
      vars,
      dataContactCore,
    );
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send({code: 200, data: response});
  } catch (err) {
    res.status(500).send({code: 500, data: JSON.stringify(err)});
  }
};

export const CreateContactHandler = async (req: $Request, res: $Response) => {
  const dataContactCore: interfaces.ContactCore = req.body;
  try {
    const response = await createContact(
      dataContactCore,
    );
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send({code: 200, data: response});
  } catch (err) {
    res.status(500).send({code: 500, data: JSON.stringify(err)});
  }
};

export const DeleteContactHandler = async (req: $Request, res: $Response) => {
  let vars = {};
  vars["contact_id"] = req.params["contact_id"];
  try {
    const response = await deleteContact(
      vars,
    );
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send({code: 200, data: response});
  } catch (err) {
    res.status(500).send({code: 500, data: JSON.stringify(err)});
  }
};



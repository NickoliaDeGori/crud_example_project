/* @flow */
import express from 'express';
import * as handlers from './handlers';

let router = express.Router();

router.get("/api/v1/contacts/all", handlers.GetAllContactsHandler);
router.get("/api/v1/contacts/get/:contact_id", handlers.GetContactHandler);
router.post("/api/v1/contacts/update/:contact_id", handlers.UpdateContactHandler);
router.post("/api/v1/contacts/create", handlers.CreateContactHandler);
router.delete("/api/v1/contacts/delete/:contact_id", handlers.DeleteContactHandler);

export default router;

/* @flow */
import * as interfaces from "../../types/interfaces"
import ContactSchema from "../../database/scheme/Contact"
import {DB_ERROR, INPUT_ERROR} from "../../constants/errors";
import {testName, testPhone} from "../../utilits/inputs";

export default (dataContactCore: interfaces.ContactCore,) => {
  return new Promise(async (resolve, reject) => {

    let newContact = new ContactSchema();

    if (!testName(dataContactCore.first_name) ||
        !testPhone(dataContactCore.phone)) {
      const errorObj: interfaces.Failure = {
        code: INPUT_ERROR,
        message: `Введенные данные некорректны`
      };
      reject(errorObj)
    }

    try {
      newContact.first_name = dataContactCore.first_name;
      newContact.last_name = dataContactCore.last_name;
      newContact.phone = dataContactCore.phone;
      newContact.address = dataContactCore.address;
      newContact.email = dataContactCore.email;
      newContact.setId();

      await newContact.save();

      const responseContact: interfaces.Contact = {
        ...dataContactCore,
        id: newContact.id,
      };

      resolve(responseContact);
    } catch (err) {
      const errorObj: interfaces.Failure = {
        code: DB_ERROR,
        message: `Что-то пошло не так: ${err.toString()}`
      };
      reject(errorObj)
    }
  });
}

/* @flow */
import * as interfaces from "../../types/interfaces"
import ContactSchema from "../../database/scheme/Contact";
import {DB_ERROR} from "../../constants/errors";

export default (vars: { [string]: any },) => {
  return new Promise(async (resolve, reject) => {
    try {
      const responseContact: interfaces.Contact = await ContactSchema.findOne({
        id: vars["contact_id"]
      }).exec();

      resolve(responseContact);
    } catch (err) {
      const errorObj: interfaces.Failure = {
        code: DB_ERROR,
        message: `Что-то пошло не так: ${err.toString()}`
      };
      reject(errorObj)
    }
  });
}

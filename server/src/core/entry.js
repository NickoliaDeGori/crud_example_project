/* @flow */
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import router from '../routers/routers';

const port = process.env.PORT || 8020;
const server = express();

server.use(cors());
server.use(bodyParser.json());
server.use('/', router);

server.listen(port, () => {
  console.log('App listening on port', port);
});

export default server;

import ContactSchema from '../database/scheme/Contact';
import faker from 'faker';

function generateContact (local: string) {
  faker.locale = local;
  return new Promise(async (resolve) => {
    for (let i = 1; i <= 50; i++) {
      let newContact = new ContactSchema();
      newContact.first_name = faker.name.firstName();
      newContact.last_name = faker.name.lastName();
      newContact.phone = faker.phone.phoneNumber();
      newContact.email= faker.internet.email();
      newContact.address = `${faker.address.city()} ${faker.address.streetAddress()}`;
      newContact.setId();
      console.log(newContact);

      await newContact.save();
    }
    resolve();
  })
}

(async () => {
  await generateContact("ru");
  await generateContact("en");
  console.log('Generation complete!');
  process.exit(0);
})();

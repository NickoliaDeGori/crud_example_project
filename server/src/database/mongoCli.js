import mongoose from 'mongoose';
import {mongoBase, mongoHost, mongoPort} from '../options/db';

const client = mongoose.createConnection(`mongodb://${mongoHost}:${mongoPort}/${mongoBase}`);

export default client;

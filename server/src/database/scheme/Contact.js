import mongoClient  from '../mongoCli';
import mongoose from "mongoose";
import cyrillicToTranslit from "../../utilits/cyrillicToTranslit";

let ContactSchema = new mongoose.Schema({
  id: {type: String, required: false},
  first_name: {type: String, required: true},
  last_name: {type: String, required: false},
  phone: {type: String, required: true},
  address: {type: String, required: false},
  email: {type: String, required: false},
});

ContactSchema.methods.setId = function(){
  this.id = cyrillicToTranslit(this.first_name) + "_" + Date.now();
};

ContactSchema.methods.updateData = function(newContactData){
  this.first_name = !!newContactData.first_name ? newContactData.first_name : this.first_name;
  this.last_name = !!newContactData.last_name ? newContactData.last_name : this.last_name;
  this.phone = !!newContactData.phone ? newContactData.phone : this.phone;
  this.address = !!newContactData.address ? newContactData.address : this.address;
  this.email = !!newContactData.email ? newContactData.email : this.email;
};


export default mongoClient.model('contact', ContactSchema);

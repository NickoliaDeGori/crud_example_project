// @flow
export const testName = (name: string): boolean => {
  return !!name && name.length >= 2;
};

export const testPhone = (name: string): boolean => {
  return !!name && name.length >= 2;
};

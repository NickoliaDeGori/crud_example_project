/* @flow */
  export type Contact = {
    id?: string,
    first_name?: string,
    last_name?: string,
    phone?: string,
    address?: string,
    email?: string,
  }

  export type ContactCore = {
    first_name?: string,
    last_name?: string,
    phone?: string,
    address?: string,
    email?: string,
  }

  export type Contacts = {
    count?: number,
    contacts?: Contact[],
  }

  export type Success = {
    code?: number,
  }

  export type Failure = {
    code?: number,
    messages?: string,
  }


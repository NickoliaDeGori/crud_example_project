import api from "../api";
import * as interfaces from "./interface";

export const actionGetAllContacts = async (
  
): Promise<interfaces.Contacts> => {

  return new Promise((resolve, reject) => {
    let headers = {};
    try {
      api({
        url: '/api/v1/contacts/all',
        method: 'get',
        port: '8020',
        headers: headers,
      })
        .then((response: interfaces.Contacts) => {
          resolve(response);
        })
        .catch(function (error) {
          reject(error);
        });
    } catch(error) {
      reject(error);
    }
  });
};

export const actionGetContact = async (
    { contact_id, },
  
): Promise<interfaces.Contact> => {

  return new Promise((resolve, reject) => {
    let headers = {};
    try {
      api({
        url: '/api/v1/contacts/get/:contact_id',
        method: 'get',
        port: '8020',
        headers: headers,
        urlParams: {
          contact_id,
        },
      })
        .then((response: interfaces.Contact) => {
          resolve(response);
        })
        .catch(function (error) {
          reject(error);
        });
    } catch(error) {
      reject(error);
    }
  });
};

export const actionUpdateContact = async (
    { contact_id, },
    dataBody,
  
): Promise<interfaces.Contact> => {

  return new Promise((resolve, reject) => {
    let headers = {};
    try {
      api({
        url: '/api/v1/contacts/update/:contact_id',
        method: 'post',
        port: '8020',
        headers: headers,
        urlParams: {
          contact_id,
        },
        body: dataBody,
      })
        .then((response: interfaces.Contact) => {
          resolve(response);
        })
        .catch(function (error) {
          reject(error);
        });
    } catch(error) {
      reject(error);
    }
  });
};

export const actionCreateContact = async (
    dataBody,
  
): Promise<interfaces.Contact> => {

  return new Promise((resolve, reject) => {
    let headers = {};
    try {
      api({
        url: '/api/v1/contacts/create',
        method: 'post',
        port: '8020',
        headers: headers,
        body: dataBody,
      })
        .then((response: interfaces.Contact) => {
          resolve(response);
        })
        .catch(function (error) {
          reject(error);
        });
    } catch(error) {
      reject(error);
    }
  });
};

export const actionDeleteContact = async (
    { contact_id, },
  
): Promise<interfaces.Success> => {

  return new Promise((resolve, reject) => {
    let headers = {};
    try {
      api({
        url: '/api/v1/contacts/delete/:contact_id',
        method: 'delete',
        port: '8020',
        headers: headers,
        urlParams: {
          contact_id,
        },
      })
        .then((response: interfaces.Success) => {
          resolve(response);
        })
        .catch(function (error) {
          reject(error);
        });
    } catch(error) {
      reject(error);
    }
  });
};


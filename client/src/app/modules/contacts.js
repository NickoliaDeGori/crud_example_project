import * as interfaces from "../../api/contacts/interface";

export const ADD_CONTACTS = 'contacts/ADD_CONTACTS';
export const ADD_CONTACT = 'contacts/ADD_CONTACT';
export const REMOVE_CONTACT = 'contacts/REMOVE_CONTACT';

const initialState: interfaces.Contact[] = [];

export default (state:interfaces.Contact[] = initialState, action) => {
  switch (action.type) {
    case ADD_CONTACTS:
      return [...action.contacts];
    case ADD_CONTACT:
      return [
        ...state,
        action.contact
      ];
    case REMOVE_CONTACT:
      const contacts = state.filter((contact) => contact.id !== action.id);
      return [...contacts];
    default:
      return state;
  }
}

export const addContacts = (contacts: interfaces.Contact[]) => {
  return dispatch => {
    dispatch({
      type: ADD_CONTACTS,
      contacts,
    });
  }
};

export const addContact = (contact: interfaces.Contact) => {
  return dispatch => {
    dispatch({
      type: ADD_CONTACT,
      contact,
    });
  }
};

export const removeContact = (id: string) => {
  return dispatch => {
    dispatch({
      type: REMOVE_CONTACT,
      id: id,
    });
  }
};

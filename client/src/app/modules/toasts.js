export const ADD_TOAST = 'toasts/ADD_TOAST';
export const REMOVE_TOAST = 'contacts/REMOVE_TOAST';

export type TToast = {
  text: string,
  id: number,
}

const initialState: TToast[] = [];

export default (state:TToast[] = initialState, action) => {
  switch (action.type) {
    case ADD_TOAST:
      return [
        ...state,
        { text: action.text, id: Date.now()}
      ];
    case REMOVE_TOAST:
      const toasts = state.filter((toast) => toast.id !== action.id);
      return [...toasts];
    default:
      return state;
  }
}

export const addToast = (text: string) => {
  return dispatch => {
    dispatch({
      type: ADD_TOAST,
      text,
    });
  }
};

export const removeToast = (id: string) => {
  return dispatch => {
    dispatch({
      type: REMOVE_TOAST,
      id: id,
    });
  }
};

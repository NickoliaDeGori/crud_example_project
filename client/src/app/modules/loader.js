export const SHOW_LOADER = 'toasts/SHOW_LOADER';
export const HIDE_LOADER = 'contacts/HIDE_LOADER';

const initialState: boolean = true;

export default (state:boolean = initialState, action) => {
  switch (action.type) {
    case SHOW_LOADER:
      return true;
    case HIDE_LOADER:
      return false;
    default:
      return state;
  }
}

export const showLoader = () => {
  return dispatch => {
    dispatch({
      type: SHOW_LOADER,
    });
  }
};

export const hideLoader = () => {
  return dispatch => {
    dispatch({
      type: HIDE_LOADER,
    });
  }
};

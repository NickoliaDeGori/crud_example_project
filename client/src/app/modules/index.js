import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'
import contacts from "./contacts";
import toasts from "./toasts";
import loader from "./loader";

export default combineReducers({
  routing: routerReducer,
  contacts: contacts,
  toasts: toasts,
  loader: loader,
})

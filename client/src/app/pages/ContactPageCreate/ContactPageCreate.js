/* @flow */
import React, {Component} from 'react';
import {testName, testPhone} from "../../../utils/inputs";
import InputBlock from "../../components/InputBlock/InputBlock";
import {Button, Form, Scroll} from "./ContactPageCreate.styled";
import * as interfaces from "../../../api/contacts/interface";

type ContactPageCreateProps = {
  createContact: (contact: interfaces.Contact) => Promise<{error: ?{text: string, code: ?number}}>,
  goTo: (page: string) => void,
  sendErrorAlert: () => void,
  hideLoader: () => void
};

type ContactPageCreateState = {
  first_name_error: string,
  phone_error: string,
  first_name: string,
  last_name: string,
  phone: string,
  address: string,
  email: string,
  isButtonDisabled: boolean,
};

class ContactPageCreate extends Component<ContactPageCreateProps, ContactPageCreateState> {

  state = {
    first_name_error: "",
    phone_error: "",
    first_name: "",
    last_name: "",
    phone: "",
    address: "",
    email: "",
    isButtonDisabled: true,
  };

  componentDidMount() {
    this.props.hideLoader()
  }

  validateInput = (name: string, value: string): string => {
    if (name !== 'first_name' && name !== 'phone') return '';
    switch (name) {
      case 'first_name':
        return !testName(value) ? 'Имя должно быть длиинной не менее 2-х символов' : '';
      case 'phone':
        return !testPhone(value) ? 'Номер телефона должен быть длиинной не менее 2-х символов' : '';
      default:
        return ''
    }
  };

  onClickSave = (event: SyntheticInputEvent<EventTarget>) => {
    event.preventDefault();
    if (!this.isInputsValid) {
      this.setState({
        first_name_error: this.validateInput('first_name', this.state.first_name),
        phone_error: this.validateInput('password', this.state.phone),
      });
    } else {
      this.props.createContact({
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        phone: this.state.phone,
        address: this.state.address,
        email: this.state.email,
      }).then(({error}) => {
        if (error !== null && error !== undefined) {
          this.props.sendErrorAlert();
        }
      });
    }
  };

  onChangeText = (name: string, val: SyntheticInputEvent<EventTarget>) => {
    const newState = {
      [name]: val.target.value,
    };

    if (name === 'first_name' || name === 'phone') {
      newState[`${name}_error`] = this.validateInput(name, val.target.value)
    }

    const isInputsValid = this.isInputsValid({
      ...this.state,
      ...newState,
    });
    this.setState({
      ...newState,
      isButtonDisabled: !isInputsValid,
    });
  };

  isInputsValid = (newState: ContactPageCreateState) => {
    return newState.first_name.length >= 2 && newState.phone.length > 2
  };

  render() {
    return (
        <Scroll>
          <Form onSubmit={this.onClickSave}>
            <InputBlock
                name="first_name"
                placeholder="First Name"
                onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("first_name", val)}
                value={this.state.first_name}
                error={this.state.first_name_error}
            />
            <InputBlock
                name="last_name"
                placeholder="Last Name"
                onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("last_name", val)}
                value={this.state.last_name}
            />
            <InputBlock
                name="phone"
                placeholder="Phone"
                onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("phone", val)}
                value={this.state.phone}
                error={this.state.phone_error}
            />
            <InputBlock
                name="email"
                placeholder="E-mail"
                onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("email", val)}
                value={this.state.email}
            />
            <InputBlock
                name="address"
                placeholder="Address"
                onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("address", val)}
                value={this.state.address}
            />
            <Button
                id="submit_button"
                type='submit'
                disabled={this.state.isButtonDisabled}
            >
              Create
            </Button>
          </Form>
        </Scroll>
    );
  }
}

export default ContactPageCreate

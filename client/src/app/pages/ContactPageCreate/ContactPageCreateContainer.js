/* @flow */
import {
  actionCreateContact,
} from "../../../api/contacts/requests";
import {push} from "react-router-redux";
import {connect} from "react-redux";
import {addContact} from "../../modules/contacts";
import {addToast} from "../../modules/toasts";
import ContactPageCreate from "./ContactPageCreate";
import {hideLoader, showLoader} from "../../modules/loader";

const mapDispatchToProps = (dispatch) => ({
  hideLoader: () => {
    dispatch(hideLoader());
  },
  createContact: async (newContact) => {
    dispatch(showLoader());
    try {
      let contact = await actionCreateContact(newContact);
      dispatch(addContact(contact.data));
      dispatch(addToast("Контакт создан"));
      dispatch(push("/"));
      dispatch(hideLoader());
      return {
        error: null,
      };
    } catch (err) {
      dispatch(hideLoader());
      return {
        data: null,
        error: err,
      };
    }
  },
  goTo: (page: string) => {
    dispatch(push(page));
  },
  sendErrorAlert: () => {
    dispatch(addToast("Что-то пошло не так"));
  },
});

export default connect(
    null,
    mapDispatchToProps,
)(ContactPageCreate)

/* @flow */
import React, {Component} from 'react';
import {
  Arrow,
  Button,
  ContractWrapper,
  Header,
  HeaderSearch,
  Item,
  Label,
  Scroll,
  Wrapper
} from "./HomePage.styled";
import * as interfaces from "../../../api/contacts/interface";
import InputBlock from "../../components/InputBlock/InputBlock";
import Popup from "../../components/Popup/PopupContainer";

type HomePageProps = {
  contacts: interfaces.Contact[],
  hideLoader: () => void
};

type HomePageState = {
  search: string,
  isReverse: boolean,
  showPopup: boolean,
};

class HomePage extends Component<HomePageProps, HomePageState> {

  state = {
    search: "",
    showPopup: false,
    isReverse: false,
  };

  componentDidMount() {
    this.props.hideLoader()
  }

  toggleVisible = () => {
    this.setState({showPopup: !this.state.showPopup})
  };

  onChangeText = (name: string, val: SyntheticInputEvent<EventTarget>) => {
    const newState = {
      [name]: val.target.value,
    };

    this.setState({
      ...newState,
    });
  };

  setReverse = () => {
    this.setState({
      isReverse: !this.state.isReverse,
    });
  };

  get contacts() {
    const contactsLabel = {};
    const contractsElem = [];
    this.props.contacts.forEach((contact: interfaces.Contact) => {
      if (!!this.state.search &&
          (typeof contact.first_name === "string" && contact.first_name.indexOf(this.state.search) < 0) &&
          (typeof contact.last_name === "string" && contact.last_name.indexOf(this.state.search) < 0) &&
          (typeof contact.phone === "string" && contact.phone.indexOf(this.state.search) < 0)
      ) return;


      if (typeof contact.first_name === "string") {
        const label: string = contact.first_name.charAt(0).toUpperCase();
        if (!contactsLabel[label]) {
          contactsLabel[label] = []
        }
        contactsLabel[label].push(contact);
      }
    });

    Object.keys(contactsLabel).sort((a, b) => a.localeCompare(b)).forEach((label, index) => {
      const contracts = contactsLabel[label].sort(
          (a, b) => a.first_name.localeCompare(b.first_name)
      );
      contractsElem.push(
          <ContractWrapper key={`${label}-${index}`}>
            <Label>{label}</Label>
            {contracts.map((contract) => (
                <Item to={"/contact/" + contract.id} key={`${contract.id}`}>
                  {contract.first_name} {contract.last_name}
                </Item>
            ))}
          </ContractWrapper>
      )
    });
    if (this.state.isReverse) {
      return contractsElem.reverse();
    }
    return contractsElem;
  }

  render() {
    return (
        <Wrapper>
          <Header>

            <Button
                id="submit_button"
                onClick={this.toggleVisible}
            >
              +
            </Button>
            <HeaderSearch>
              <InputBlock
                  name="search"
                  placeholder="Find contact"
                  onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("search", val)}
                  value={this.state.search}
              />
            </HeaderSearch>
            <Arrow onClick={this.setReverse} isReverse={this.state.isReverse}/>
          </Header>
          <Scroll>
            {this.contacts}
          </Scroll>
          <Popup isShow={this.state.showPopup}
                 toggleVisible={this.toggleVisible}/>
        </Wrapper>
    );
  }
}

export default HomePage

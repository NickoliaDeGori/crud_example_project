/* @flow */
import {connect} from "react-redux";
import HomePage from "./HomePage";
import {hideLoader} from "../../modules/loader";

const mapStateToProps = state => ({
  contacts: state.contacts,
});

const mapDispatchToProps = dispatch => ({
  hideLoader: () => {
    dispatch(hideLoader());
  }
});
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(HomePage)

import styled from "styled-components";
import arrowIcon from '../../images/arrow.svg';
import {Link} from "react-router-dom";

export const Wrapper = styled.section`
    width: 100%;
    height: 100%;
  `;
export const Header = styled.section`
    width: 100%;
    height: 20px;
    display: flex;
    justify-content: space-between;
    align-items: center;
  `;
export const HeaderSearch = styled.section`
    width: 85%;
  `;
export const Arrow  = styled.section`
    width: 40px;
    height: 40px;
    background-color: rgba(255,255,255, 1);
    mask: url(${arrowIcon}) no-repeat 50% 50%;
    mask-size: 20px;
    cursor: pointer;
    transform: rotate(${
      props => !props.isReverse ? '180deg' : '0deg'
    });
    position: relative;
    top: -5px;
  `;

export const Scroll = styled.section`
    width: 100%;
    height: calc(100% - 53px);
    overflow-y: auto;
    box-sizing: border-box;
    margin-top: 10px;
    padding: 0 10px;
    ::-webkit-scrollbar {
        width: 3px;
    }
    
    ::-webkit-scrollbar-track {
        background: #888; 
    }
    
    ::-webkit-scrollbar-thumb {
        background: #f1f1f1; 
    }
    
    ::-webkit-scrollbar-thumb:hover {
        background: #555; 
    }
`;
export const ContractWrapper = styled.section`

`;
export const Label = styled.h3`
    color: #fff;
    font-size: 18px;
    height: 40px;
    border-bottom: 2px solid rgba(255,255,255,0.2);
`;
export const Item = styled(Link)`
    height: 30px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: 16px;
    color: #fff;
    text-decoration: none;
`;

export const Button = styled.button`
      position: relative;
      display: block;
      width: 30px;
      height: 30px;
      color: rgba(255,255,255,0.8);
      background: #29b9ac;
      font-size: 16px;
      border-radius: 30px;
      cursor: pointer;
      overflow: hidden;    
      margin-right: 10px;
      line-height: 25px;
      transition: width 0.3s 0.15s, font-size 0.1s 0.15s;
      position: relative;
      top: -5px;
      :focus {
        outline: none;
      }
      :active {
        outline: none;
      }
      :disabled {
        background: #a485bf;
      }
  `;

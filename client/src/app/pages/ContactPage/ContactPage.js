/* @flow */
import React, {Component} from 'react';
import {isEqual} from 'lodash';
import {testName, testPhone} from "../../../utils/inputs";
import InputBlock from "../../components/InputBlock/InputBlock";
import {Button, Form, Scroll} from "./ContactPage.styled";
import * as interfaces from "../../../api/contacts/interface";

type ContactPageProps = {
  getContact: () => Promise<{error: ?{text: string, code: number}, data: interfaces.Contact}>,
  updateContact: (contact: interfaces.Contact) => Promise<{error: ?{text: string, code: number}, data: interfaces.Contact}>,
  removeContact: () => Promise<{error: ?{text: string, code: number}}>,
  goTo: (page: string) => void,
  sendErrorAlert: () => void,
  match: {params: {id: string}},
  hideLoader: () => void
};

type ContactPageState = {
  first_name_error: string,
  phone_error: string,
  first_name: string,
  last_name: string,
  phone: string,
  address: string,
  email: string,
  first_name_editable: boolean,
  last_name_editable: boolean,
  phone_editable: boolean,
  address_editable: boolean,
  email_editable: boolean,
  isButtonDisabled: boolean,
};

class ContactPage extends Component<ContactPageProps, ContactPageState> {

  state = {
    first_name_error: "",
    phone_error: "",
    first_name: "",
    last_name: "",
    phone: "",
    address: "",
    email: "",
    first_name_editable: false,
    last_name_editable: false,
    phone_editable: false,
    address_editable: false,
    email_editable: false,
    isButtonDisabled: true,
  };

  contactCurrent = {};

  componentDidMount() {
    this.props.hideLoader();
    this.props.getContact()
        .then(({data, error}) => {
          if (data === null || error !== null) {
            this.props.sendErrorAlert();
            return;
          }
          this.saveContact(data);
        });
  }

  saveContact = (contact: interfaces.Contact) => {
    this.contactCurrent = {
      first_name: contact.first_name,
      last_name: contact.last_name,
      phone: contact.phone,
      address: contact.address,
      email: contact.email,
    };
    this.setState({
      ...this.contactCurrent,
      isButtonDisabled: true,
    });
  };

  onClickRemove = () => {
    this.props.removeContact()
        .then(({error}) => {
          if (error !== null && error !== undefined) {
            this.props.sendErrorAlert();
          }
        });
  };

  onClickSave = (event: SyntheticInputEvent<EventTarget>) => {
    event.preventDefault();
    if (!this.isInputsValid) {
      this.setState({
        first_name_error: this.validateInput('first_name', this.state.first_name),
        phone_error: this.validateInput('password', this.state.phone),
      });
    } else {
      this.props.updateContact({
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        phone: this.state.phone,
        address: this.state.address,
        email: this.state.email,
        id: this.props.match.params.id,
      }).then(({data, error}) => {
        if (error !== null && error !== undefined) {
          this.props.sendErrorAlert();
          return;
        }
        this.saveContact(data);
      });
    }
  };

  onEditText = (name: string) => {
    this.setState({
      [`${name}_editable`]: !this.state[`${name}_editable`]
    });
  };

  validateInput = (name: string, value: string): string => {
    if (name !== 'first_name' && name !== 'phone') return '';
    switch (name) {
      case 'first_name':
        return !testName(value) ? 'Имя должно быть длиинной не менее 2-х символов' : '';
      case 'phone':
        return !testPhone(value) ? 'Номер телефона должен быть длиинной не менее 2-х символов' : '';
      default:
        return ''
    }
  };

  onChangeText = (name: string, val: SyntheticInputEvent<EventTarget>) => {
    const newState = {
      [name]: val.target.value,
    };

    if (name === 'first_name' || name === 'phone') {
      newState[`${name}_error`] = this.validateInput(name, val.target.value)
    }

    const isInputsValid = this.isInputsValid({
      ...this.state,
      ...newState,
    });

    this.setState({
      ...newState,
      isButtonDisabled: !isInputsValid,
    });
  };

  isInputsValid = (newState: ContactPageState) => {
    return newState.first_name.length >= 2 && newState.phone.length > 2 && !isEqual(
        this.contactCurrent,
        {
          first_name: this.state.first_name,
          last_name: this.state.last_name,
          phone: this.state.phone,
          address: this.state.address,
          email: this.state.email,
        }
    )
  };

  render() {
    return (
        <Scroll>
          <Form onSubmit={this.onClickSave}>
            <InputBlock
                name="first_name"
                placeholder="First Name"
                onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("first_name", val)}
                value={this.state.first_name}
                error={this.state.first_name_error}
                isEdit={true}
                isEditable={this.state.first_name_editable}
                onEditClick={() => this.onEditText("first_name")}
            />
            <InputBlock
                name="last_name"
                placeholder="Last Name"
                onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("last_name", val)}
                value={this.state.last_name}
                isEdit={true}
                isEditable={this.state.last_name_editable}
                onEditClick={() => this.onEditText("last_name")}
            />
            <InputBlock
                name="phone"
                placeholder="Phone"
                onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("phone", val)}
                value={this.state.phone}
                error={this.state.phone_error}
                isEdit={true}
                isEditable={this.state.phone_editable}
                onEditClick={() => this.onEditText("phone")}
            />
            <InputBlock
                name="email"
                placeholder="E-mail"
                onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("email", val)}
                value={this.state.email}
                isEdit={true}
                isEditable={this.state.email_editable}
                onEditClick={() => this.onEditText("email")}
            />
            <InputBlock
                name="address"
                placeholder="Address"
                onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("address", val)}
                value={this.state.address}
                isEdit={true}
                isEditable={this.state.address_editable}
                onEditClick={() => this.onEditText("address")}
            />
            <Button
                id="submit_button"
                type='submit'
                disabled={this.state.isButtonDisabled}
            >
              Update
            </Button>
          </Form>
          <Button
              id="submit_button"
              red={true}
              onClick={this.onClickRemove}
          >
            Remove
          </Button>
        </Scroll>
    );
  }
}

export default ContactPage

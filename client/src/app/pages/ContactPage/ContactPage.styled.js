import styled from "styled-components";

export const Scroll = styled.section`
    width: 100%;
    height: 100%;
    overflow-y: auto;
    ::-webkit-scrollbar {
        width: 3px;
    }
    
    ::-webkit-scrollbar-track {
        background: #888; 
    }
    
    ::-webkit-scrollbar-thumb {
        background: #f1f1f1; 
    }
    
    ::-webkit-scrollbar-thumb:hover {
        background: #555; 
    }
`;

export const Button = styled.button`
      position: relative;
      width: 100%;
      height: 50px;
      margin: 20px auto;
      color: rgba(255,255,255,0.8);
      background: ${props => props.red ? "#FF3366" : "#29b9ac"} ;
      font-size: 16px;
      border-radius: 50px;
      cursor: pointer;
      overflow: hidden;
      transition: width 0.3s 0.15s, font-size 0.1s 0.15s;
      :focus {
        outline: none;
      }
      :active {
        outline: none;
      }
      :disabled {
        background: #a485bf;
      }
  `;

export const Form = styled.form`
    
  `;

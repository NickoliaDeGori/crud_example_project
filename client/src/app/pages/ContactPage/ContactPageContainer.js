/* @flow */
import {push} from "react-router-redux";
import {connect} from "react-redux";
import {
  actionDeleteContact,
  actionGetContact, actionUpdateContact
} from "../../../api/contacts/requests";
import {removeContact} from "../../modules/contacts";
import {addToast} from "../../modules/toasts";
import ContactPage from "./ContactPage";
import * as interfaces from "../../../api/contacts/interface";
import {hideLoader, showLoader} from "../../modules/loader";

const mapDispatchToProps = (dispatch, ownProps) => ({
  hideLoader: () => {
    dispatch(hideLoader());
  },
  getContact: async () => {
    dispatch(showLoader());
    try {
      let contact = await actionGetContact({contact_id: ownProps.match.params.id});
      dispatch(hideLoader());
      return {
        data: contact.data,
        error: null,
      };
    } catch (err) {
      dispatch(hideLoader());
      return {
        data: null,
        error: err,
      };
    }
  },
  updateContact: async (newContact: interfaces.Contact) => {
    dispatch(showLoader());
    try {
      let contact = await actionUpdateContact({contact_id: ownProps.match.params.id}, newContact);
      dispatch(hideLoader());
      dispatch(addToast("Контакт обновлен"));
      return {
        data: contact.data,
        error: null,
      };
    } catch (err) {
      dispatch(hideLoader());
      return {
        data: null,
        error: err,
      };
    }
  },
  removeContact: async () => {
    dispatch(showLoader());
    try {
      await actionDeleteContact({contact_id: ownProps.match.params.id});
      dispatch(removeContact(ownProps.match.params.id));
      dispatch(addToast("Контакт удален"));
      dispatch(push("/"));
      dispatch(hideLoader());
      return {
        error: null,
      };
    } catch (err) {
      dispatch(hideLoader());
      return {
        error: err,
      };
    }
  },
  goTo: (page: string) => {
    dispatch(push(page));
  },
  sendErrorAlert: () => {
    dispatch(addToast("Что-то пошло не так"));
  },
});
export default connect(
    null,
    mapDispatchToProps,
)(ContactPage)

import styled from "styled-components";

export const Wrapper = styled.section`
    width: 100%;
    height: 100%;
  `;

export const Scroll = styled.section`
    width: 100%;
    height: calc(100% - 43px);
    overflow-y: auto;
    ::-webkit-scrollbar {
        width: 3px;
    }
    
    ::-webkit-scrollbar-track {
        background: #888; 
    }
    
    ::-webkit-scrollbar-thumb {
        background: #f1f1f1; 
    }
    
    ::-webkit-scrollbar-thumb:hover {
        background: #555; 
    }
`;

export const Title = styled.h1`
    margin-top: 0;
    color: #ffffff;
    font-size: 20px;
    text-align: center;
  `;

export const Paragraph = styled.p`
    color: #ffffff;
    font-size: 12px;
  `;


/* @flow */
import {connect} from "react-redux";
import {hideLoader} from "../../modules/loader";
import AboutUsPage from "./AboutUsPage";

const mapDispatchToProps = dispatch => ({
  hideLoader: () => {
    dispatch(hideLoader());
  }
});

export default connect(
    null,
    mapDispatchToProps,
)(AboutUsPage)

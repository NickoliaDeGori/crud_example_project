/* @flow */
import React, {Component} from 'react';
import {Paragraph, Scroll, Title, Wrapper} from "./AboutUsPage.styled";

type AboutUsPageProps = {
  hideLoader: () => void
};


class AboutUsPage extends Component<AboutUsPageProps> {

  componentDidMount() {
    this.props.hideLoader()
  }

  render() {
    return (
        <Wrapper>
          <Title>О приложении</Title>
          <Scroll>
            <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              Proin condimentum semper urna, ac fringilla arcu pretium ut. Vivamus
              in urna id dolor convallis ullamcorper. Integer pulvinar maximus
              massa. Quisque vel orci cursus, rhoncus nisi vel, viverra nulla.
              Nunc et venenatis mi, vel laoreet augue. Duis at tortor sed lacus
              faucibus mattis eu dignissim purus. Phasellus sagittis congue massa
              et venenatis. Sed ac felis nec orci condimentum imperdiet sed et
              urna. Etiam porttitor ultricies nibh nec fermentum.</Paragraph>
            <Paragraph>Pellentesque vulputate ligula at vehicula aliquam.
              Pellentesque nec augue mauris. Quisque non vehicula neque. Maecenas
              molestie magna vitae eros scelerisque, sit amet varius turpis
              elementum. Nullam vulputate vel metus eget bibendum. Morbi et odio
              vitae justo posuere condimentum. Nullam efficitur neque vel eros
              mollis, vulputate tristique massa elementum.</Paragraph>
            <Paragraph>Phasellus ac sodales mauris, quis tincidunt ipsum. Donec
              posuere mollis ante, in congue magna. Nunc a augue ipsum. Etiam
              ultrices massa sit amet mauris varius sollicitudin. Donec ut purus
              dui. Aliquam sit amet augue quis urna condimentum posuere vitae id
              tellus. Pellentesque pulvinar leo sit amet euismod mollis. Nunc
              viverra dictum erat vitae dignissim. Fusce ullamcorper elit blandit
              egestas elementum.</Paragraph>
            <Paragraph>Phasellus porta pulvinar placerat. Maecenas varius felis
              sit amet lacus volutpat faucibus. Curabitur id auctor nulla. Nullam
              non ipsum vel ex viverra interdum nec et nibh. Nulla viverra mi sed
              dui venenatis, in congue velit fermentum. Pellentesque in felis
              scelerisque, porttitor lectus in, hendrerit lectus. Integer ac
              malesuada nunc, tempor maximus nunc. Etiam rutrum ligula massa, quis
              ultrices eros mattis vel. Ut a sodales est, eu hendrerit
              tortor.</Paragraph>
            <Paragraph>Sed mollis imperdiet libero nec suscipit. Curabitur
              eleifend magna ante, sit amet fringilla est ornare at. Pellentesque
              eleifend mi quis ipsum laoreet blandit. Donec venenatis commodo
              suscipit. Duis euismod finibus orci, sit amet euismod libero
              venenatis volutpat. Nullam sollicitudin erat sit amet iaculis
              consequat. Nullam vitae enim ut neque tempus rutrum sed vel enim.
              Praesent id nunc erat. Nulla ut magna neque. Nulla facilisi. Cras
              gravida tincidunt dignissim. Aliquam egestas facilisis nunc, a
              facilisis libero.</Paragraph>
          </Scroll>
        </Wrapper>
    );
  }
}

export default AboutUsPage;

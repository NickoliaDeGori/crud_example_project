/* @flow */
import React, {Component} from 'react';
import logo from './images/logo.svg';
import {Link, Route, withRouter} from 'react-router-dom'
import {Footer, Header, Image, Intro, Wrapper} from "./App.styled";
import HomePage from "./pages/HomePage/HomePageContainer";
import AboutUsPage from "./pages/AboutUsPage/AboutUsPageContainer";
import ContactPage from "./pages/ContactPage/ContactPageContainer";
import ContactPageCreate from "./pages/ContactPageCreate/ContactPageCreateContainer";
import {actionGetAllContacts} from "../api/contacts/requests";
import {addContacts} from "./modules/contacts";
import {connect} from "react-redux";
import Loader from "./components/Loader/LoaderContainer";
import Toasts from "./components/Toasts/Toasts";
import {hideLoader, showLoader} from "./modules/loader";
import {addToast} from "./modules/toasts";

type AppProps = {
  loadContacts: () => void
};

class App extends Component<AppProps> {

  componentDidMount() {
    this.props.loadContacts();
  }

  render() {
    return (
        <Wrapper>
          <Header>
            <Link to={"/"}>
              <Image src={logo} alt="logo"/>
            </Link>
            <h1>Welcome to CRUD example</h1>
          </Header>
          <Loader />
          <Intro>
            <Route exact path="/" component={HomePage}/>
            <Route exact path="/about-us" component={AboutUsPage}/>
            <Route exact path="/new-contact" component={ContactPageCreate}/>
            <Route exact path="/contact/:id" component={ContactPage}/>
          </Intro>
          <Toasts />
          <Footer>
            <Link to={"/about-us"}>О приложении</Link>
          </Footer>
        </Wrapper>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  loadContacts: async () => {
    dispatch(showLoader());
    try {
      const contacts = await actionGetAllContacts();
      dispatch(addContacts(contacts.data.contacts));
      dispatch(hideLoader());
    } catch (err) {
      dispatch(hideLoader());
      dispatch(addToast("Что-то пошло не так"));
    }
  }
});

export default withRouter(connect(
    null,
    mapDispatchToProps
)(App))

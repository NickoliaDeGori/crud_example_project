import styled from "styled-components";
import closeIcon from "../../../../images/close.svg"

export const ToastWrapper = styled.div`
    align-items: center;
    justify-content: space-between;
    border-radius: 5px;
    color: #ffffff;
    background-color: #ffffff;
    width: calc(100% - 60px);
    display: flex;
    padding: 16px;
    margin-top: 5px;
    :not(:last-child) {
      margin: 0 0 12px;
    }
`;

export const Content = styled.p`
    flex: 1 1 auto;
    margin: 0 12px 0 0;
    overflow: hidden;
    text-overflow: ellipsis;
    color: rgba(0,0,0, 1);
`;

export const Button = styled.button`
    position: relative;
    width: 30px;
    height: 30px;
    background-color: rgba(0,0,0, 1);
    mask: url(${closeIcon}) no-repeat 50% 50%;
    mask-size: 20px;
    cursor: pointer;
    z-index: 10;
`;

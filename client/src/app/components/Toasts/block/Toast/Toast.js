import React, { Component } from "react";
import {Button, Content, ToastWrapper} from "./Toast.styled";

type ToastProps = {
  onRemove: () => string,
  text: string,
  id: number,
};

class Toast extends Component<ToastProps> {
  timer;

  componentDidMount() {
    this.timer = setTimeout(() => this.props.onRemove(), 3000)
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  render() {
    return (
        <ToastWrapper>
          <Content>
            {this.props.text}
          </Content>
          <Button onClick={this.props.onRemove} />
        </ToastWrapper>
    );
  }

  shouldComponentUpdate() {
    return false;
  }
}

export default Toast;

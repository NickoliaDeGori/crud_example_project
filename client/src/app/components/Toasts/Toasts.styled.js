import styled from "styled-components";

export const ToastsWrapper = styled.div`
    bottom: 30px;
    position: fixed;
    right: 0;
    width: 100%;
    padding-left: 0;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    flex-direction: column;
    z-index: 300;
`;

import React from "react";
import { connect } from "react-redux";
import {ToastsWrapper} from "./Toasts.styled";
import Toast from "./block/Toast/Toast";
import {removeToast, TToast} from "../../modules/toasts";

type ToastsProps = {
  toasts: TToast[],
  removeToast: (number) => void
};

const Toasts = ({ removeToast, toasts }: ToastsProps) => {
  return (
      <ToastsWrapper>
        {toasts.map(toast => {
          const { id } = toast;
          return (
              <Toast {...toast} key={id} onRemove={() => removeToast(id)} />
          );
        })}
      </ToastsWrapper>
  );
};

const mapStateToProps = state => ({
  toasts: state.toasts
});

const mapDispatchToProps = dispatch => ({
  removeToast: (id) => {
    dispatch(removeToast(id))
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Toasts);

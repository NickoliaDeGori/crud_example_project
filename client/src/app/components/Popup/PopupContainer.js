/* @flow */
import {push} from "react-router-redux";
import {connect} from "react-redux";
import {actionCreateContact} from "../../../api/contacts/requests";
import {addContact} from "../../modules/contacts";
import {addToast} from "../../modules/toasts";
import Popup from "./Popup";
import {hideLoader, showLoader} from "../../modules/loader";

const mapDispatchToProps = (dispatch, ownProps) => ({
  createContact: async (newContact) => {
    dispatch(showLoader());
    try {
      let contact = await actionCreateContact(newContact);
      dispatch(addContact(contact.data));
      dispatch(addToast("Контакты добавлен"));
      ownProps.toggleVisible();
      dispatch(hideLoader());
      return {
        error: null,
      };
    } catch (err) {
      dispatch(hideLoader());
      return {
        data: null,
        error: err,
      };
    }
  },
  goTo: (page: string) => {
    dispatch(push(page));
  },
  sendErrorAlert: () => {
    dispatch(addToast("Что-то пошло не так"));
  },
});
export default connect(
    null,
    mapDispatchToProps,
)(Popup)

/* @flow */
import React, {Component} from 'react';
import {
  Button, Close, CreateLink,
  Form, Inner,
  Overlay, Title
} from "./Popup.styled";
import {testName, testPhone} from "../../../utils/inputs";
import InputBlock from "../InputBlock/InputBlock";
import * as interfaces from "../../../api/contacts/interface";

type PopupProps = {
  isShow: boolean,
  toggleVisible: () => void,
  createContact: (contact: interfaces.Contact) => Promise<{error: ?{text: string, code: number}}>,
  goTo: (page: string) => void,
  sendErrorAlert: () => void,
};

type PopupState = {
  first_name_error: string,
  phone_error: string,
  first_name: string,
  last_name: string,
  phone: string,
  address: string,
  email: string,
  isButtonDisabled: boolean,
};

class Popup extends Component<PopupProps, PopupState> {

  state = {
    first_name_error: "",
    phone_error: "",
    first_name: "",
    last_name: "",
    phone: "",
    address: "",
    email: "",
    isButtonDisabled: true,
  };

  validateInput = (name: string, value: string): string => {
    if (name !== 'first_name' && name !== 'phone') return '';
    switch (name) {
      case 'first_name':
        return !testName(value) ? 'Имя должно быть длиинной не менее 2-х символов' : '';
      case 'phone':
        return !testPhone(value) ? 'Номер телефона должен быть длиинной не менее 2-х символов' : '';
      default:
        return ''
    }
  };

  onClickSave = (event: SyntheticInputEvent<EventTarget>) => {
    event.preventDefault();
    if (!this.isInputsValid) {
      this.setState({
        first_name_error: this.validateInput('first_name', this.state.first_name),
        phone_error: this.validateInput('password', this.state.phone),
      });
    } else {
      this.props.createContact({
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        phone: this.state.phone,
        address: this.state.address,
        email: this.state.email,
      }).then(({error}) => {
        if (error !== null && error !== undefined) {
          this.props.sendErrorAlert();
        }
      });
    }
  };

  onChangeText = (name: string, val: SyntheticInputEvent<EventTarget>) => {
    const newState = {
      [name]: val.target.value,
    };

    if (name === 'first_name' || name === 'phone') {
      newState[`${name}_error`] = this.validateInput(name, val.target.value)
    }

    const isInputsValid = this.isInputsValid({
      ...this.state,
      ...newState,
    });
    this.setState({
      ...newState,
      isButtonDisabled: !isInputsValid,
    });
  };

  isInputsValid = (newState: PopupState) => {
    return newState.first_name.length >= 2 && newState.phone.length > 2
  };

  render() {
    if (!this.props.isShow) return null;
    return (
        <Overlay>
          <Inner>
            <Close onClick={this.props.toggleVisible}/>
            <Title>Создать контакт</Title>
            <Form onSubmit={this.onClickSave}>
              <InputBlock
                  name="first_name"
                  placeholder="First Name"
                  onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("first_name", val)}
                  value={this.state.first_name}
                  error={this.state.first_name_error}
              />
              <InputBlock
                  name="last_name"
                  placeholder="Last Name"
                  onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("last_name", val)}
                  value={this.state.last_name}
              />
              <InputBlock
                  name="phone"
                  placeholder="Phone"
                  onChange={(val: SyntheticInputEvent<EventTarget>) => this.onChangeText("phone", val)}
                  value={this.state.phone}
                  error={this.state.phone_error}
              />
              <Button
                  id="submit_button"
                  type='submit'
                  disabled={this.state.isButtonDisabled}
              >
                Create
              </Button>
            </Form>
            <CreateLink to={"/new-contact"}>Со всеми полями</CreateLink>
          </Inner>
        </Overlay>
    );
  }
}

export default Popup

import styled from "styled-components";
import {Link} from "react-router-dom";
import closeIcon from "../../images/close.svg"

export const Overlay = styled.section`
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0;
    left: 0;
    background-color: rgba(0,0,0,0.9);
`;

export const Inner = styled.section`
      width: 300px;
      height: auto;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%)
`;
export const Button = styled.button`
      position: relative;
      width: 100%;
      height: 50px;
      margin: 20px auto;
      color: rgba(255,255,255,0.8);
      background: #29b9ac;
      font-size: 16px;
      border-radius: 50px;
      cursor: pointer;
      overflow: hidden;
      transition: width 0.3s 0.15s, font-size 0.1s 0.15s;
      :focus {
        outline: none;
      }
      :active {
        outline: none;
      }
      :disabled {
        background: #a485bf;
      }
  `;

export const Form = styled.form`
    
  `;
export const Title = styled.h2`
    color: #ffffff;
    font-size: 20px;
    text-align: center;
    margin-bottom: 30px;
  `;

export const CreateLink = styled(Link)`
    height: 30px;
    width: 100%;
    display: block;
    font-size: 16px;
    color: #fff;
    text-decoration: none;
    text-align: center;
`;

export const Close = styled.div`
    position: absolute;
    width: 60px;
    height: 60px;
    top: -30px;
    right: -30px;
    background-color: rgba(255,255,255, 1);
    mask: url(${closeIcon}) no-repeat 50% 50%;
    mask-size: 20px;
    cursor: pointer;
    z-index: 10;
  `;

import styled from "styled-components";
import logo from '../../images/logo.svg';
import {keyframes} from "styled-components";

export const LoaderWrapper = styled.section`
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0;
    left: 0;
    background-color: #ececec;
    z-index: 2000;
`;

export const LogoSpin = keyframes`
      from {transform: translate(-50%, -50%) rotate(0deg) ; }
      to { transform: translate(-50%, -50%) rotate(360deg); }
    `;

export const LoaderImage = styled.section`
    background-image: url(${logo});
    width: 100px;
    height: 70px;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    animation: ${LogoSpin} infinite 20s linear;
`;


import * as React from 'react';
import {LoaderImage, LoaderWrapper} from "./Loader.styled";

type ILoaderProps = {
  isLoading: boolean
}

const Loader = ({isLoading}: ILoaderProps) => {
  if (!isLoading) {
    return null;
  }

  return (
      <LoaderWrapper>
        <LoaderImage/>
      </LoaderWrapper>
  );
};

export default Loader;

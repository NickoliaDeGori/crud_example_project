/* @flow */
import React from 'react';
import {Edit, ErrorTitle, Icon, Input, InputWrapper} from "./InputBlock.styled";

type InputBlockProps = {
  name: string,
  placeholder: string,
  onChange: (SyntheticInputEvent<EventTarget>) => void,
  value: string,
  error?: string,
  type?: string,
  isEdit?: boolean,
  isEditable?: boolean,
  onEditClick?: (SyntheticInputEvent<EventTarget>) => void,
};

const InputBlock = ({
                      name,
                      type = "text",
                      placeholder,
                      onChange, value,
                      error,
                      onEditClick,
                      isEdit = false,
                      isEditable = false
                    }: InputBlockProps) => {

  const onClick = (event: SyntheticInputEvent<EventTarget>) => {
    if (isEdit && !isEditable && typeof onEditClick === "function") {
      onEditClick(event)
    }
  };

  return (
      <InputWrapper isError={!!error} isEdit={isEdit} isEditable={isEditable}>
        <Icon isError={!!error} name={name}/>
        <Input
            name={name}
            type={name}
            id={name}
            placeholder={placeholder}
            value={value}
            isEdit={isEdit}
            autocomplete="off"
            isEditable={isEditable}
            onChange={onChange}
            onFocus={(event) => onClick(event)}
        />
        {!!error && (
          <ErrorTitle id={`${name}_error`}>{error}</ErrorTitle>
        )}
        {isEdit && (
          <Edit onClick={onEditClick}/>
        )}
      </InputWrapper>
  )
};

export default InputBlock;

import styled from 'styled-components';
import emailIcon from '../../images/email.svg';
import avatarIcon from '../../images/avatar.svg';
import phoneIcon from '../../images/phone.svg';
import locationIcon from '../../images/location.svg';
import searchIcon from '../../images/search.svg';
import editIcon from '../../images/edit.svg';


export const InputWrapper = styled.section`
    height: 40px;
    margin-bottom: 20px;
    border-bottom: 1px solid ${
      props => !props.isEdit || props.isEditable  ? 
          (!props.isError ? 'rgba(255,255,255,0.2)' : 'rgba(255,51,102, 1)'):
          'rgba(255,255,255,0)'
    };
    position: relative;
  `;

export const Icon = styled.div`
    position: absolute;
    width: 40px;
    height: 40px;
    top: 0;
    left: 0;
    background-color: ${
      props => !props.isError ? 'rgba(255,255,255, 1)' : 'rgba(255,51,102, 1)'
    };
    mask: url(${
      props => {
        switch (props.name) {
          case "first_name":
          case "last_name":
            return avatarIcon;
          case "phone":
            return phoneIcon;
          case "address":
            return locationIcon;
          case "email":
            return emailIcon;
          case "search":
            return searchIcon;
          default:
            return ""
        }
      }
    }) no-repeat 50% 50%;
    mask-size: 20px;
  `;

export const Edit = styled.div`
    position: absolute;
    width: 40px;
    height: 40px;
    top: 0;
    right: 0;
    background-color: rgba(255,255,255, 1);
    mask: url(${editIcon}) no-repeat 50% 50%;
    mask-size: 20px;
    cursor: pointer;
  `;

export const Input = styled.input`
    outline: none;
    border: none;
    display: inline-block;
    width: 100%;
    height: 100%;
    padding-left: 40px;
    padding-right: ${
      props => props.isEdit ? 40 : 0
    }px;
    font-size: 16px;
    background: transparent;
    color: #FDFCFD;
    box-sizing: border-box;
    &::placeholder {
      color: #FDFCFD;
    }
  `;

export const ErrorTitle = styled.div`
    width: 100%;
    font-size: 10px;
    color: #ffffff;
    background-color: rgba(255,51,102, 1);
    padding: 2px;
    box-sizing: border-box;
  `;

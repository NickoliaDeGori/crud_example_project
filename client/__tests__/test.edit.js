import React from 'react';
import PropTypes from 'prop-types';
import {mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ContactPage from '../src/app/pages/ContactPage/ContactPage';
import createRouterContext from 'react-router-test-context'

configure({ adapter: new Adapter() });

describe('======> Test Page Edit contact', () => {
  let wrapper;
  ContactPage.contextTypes = {
    router: PropTypes.object
  };

  beforeEach(()=>{
    let uniqueContact = {
      first_name: 'Kamryn',
      last_name: 'Koch',
      phone: '436.014.8802 x5584',
      email: 'Arlene19@gmail.com',
      address: 'New Alexanne 48691 Blick Court',
      id: 'Kamryn_1530467796155'
    };

    const context = createRouterContext();
    wrapper = mount(
        <ContactPage
            getContact={() => (Promise.resolve({error: null, contact: uniqueContact}))}
            updateContact={(contact) => (Promise.resolve({error: null, contact: contact}))}
            removeContact={() => (Promise.resolve({error: null}))}
            match={{params: {id: "Kamryn_1530467796155"}}}
            goTo={() => {}}
            sendErrorAlert={() => {}}
            hideLoader={() => {}}
        />,
        {context}
    );
  });

  it('Should button is disabled after render', () => {
    expect(wrapper.state().isButtonDisabled).toBeTruthy();
  });

  it('Should state have name-error and button is disabled after enter "e" to first_name input', () => {
    wrapper.instance().onChangeText("first_name", {target: {value: "e"}});
    wrapper.update();
    expect(wrapper.find("#first_name_error").exists()).toBeTruthy();
    expect(wrapper.state().isButtonDisabled).toBeTruthy();
  });

  it('Should state have phone-error and button is disabled after enter "1" to phone input', () => {
    wrapper.instance().onChangeText("phone", {target: {value: "1"}});
    wrapper.update();
    expect(wrapper.find("#phone_error").exists()).toBeTruthy();
    expect(wrapper.state().isButtonDisabled).toBeTruthy();
  });

  it('Should state have button is  not disabled after enter "+79123534534" to phone input and "Kamryn235234" to name input', () => {
    wrapper.instance().onChangeText("first_name", {target: {value: "Kamryn235234"}});
    wrapper.instance().onChangeText("phone", {target: {value: "+79123534534"}});
    wrapper.update();
    expect(wrapper.find("#first_name_error").exists()).toBeFalsy();
    expect(wrapper.find("#phone_error").exists()).toBeFalsy();
    expect(wrapper.state().isButtonDisabled).toBeFalsy();
    wrapper.instance().onClickSave({preventDefault: ()=>{}});
    wrapper.update();
    expect(wrapper.state().first_name).toEqual("Kamryn235234");
    expect(wrapper.state().phone).toEqual("+79123534534");
  });

});

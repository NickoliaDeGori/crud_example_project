import {testName, testPhone} from "../src/utils/inputs"

test('Holaba name is correct', () => {
  expect(testName("Holaba")).toBeTruthy();
});

test('H name is incorrect', () => {
  expect(testName("H")).toBeFalsy();
});

test('+78912 phone is correct', () => {
  expect(testPhone("+78912")).toBeTruthy();
});

test('+ phone is correct', () => {
  expect(testPhone("+")).toBeFalsy();
});

import thunk from 'redux-thunk'
import configureMockStore from 'redux-mock-store'
import {
  addContacts, addContact, removeContact,
  ADD_CONTACTS, ADD_CONTACT, REMOVE_CONTACT
} from '../src/app/modules/contacts';
import {
  ADD_TOAST, addToast, REMOVE_TOAST,
  removeToast
} from "../src/app/modules/toasts";
import {
  HIDE_LOADER, hideLoader, SHOW_LOADER,
  showLoader
} from "../src/app/modules/loader";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Test store - contacts', () => {

  let uniqueContact = {
    first_name: 'Kamryn',
    last_name: 'Koch',
    phone: '436.014.8802 x5584',
    email: 'Arlene19@gmail.com',
    address: 'New Alexanne 48691 Blick Court',
    id: 'Kamryn_1530467796155'
  };

  it('Test add contacts', async () => {
    const store = mockStore({contacts: []});
    await store.dispatch(addContacts([uniqueContact]));
    expect(store.getActions()[0]).toEqual({type: ADD_CONTACTS, contacts: [uniqueContact]});
  });

  it('Test add contact', async () => {
    const store = mockStore({contacts: []});
    await store.dispatch(addContact(uniqueContact));
    expect(store.getActions()[0]).toEqual({type: ADD_CONTACT, contact: uniqueContact});
  });

  it('Test remove contact', async () => {
    const store = mockStore({contacts: []});
    await store.dispatch(removeContact(uniqueContact.id));
    expect(store.getActions()[0]).toEqual({type: REMOVE_CONTACT, id: uniqueContact.id});
  });
});

describe('Test store - toast', () => {

  it('Test add toast', async () => {
    const store = mockStore({toasts: []});
    await store.dispatch(addToast("Все прекрасно"));
    expect(store.getActions()[0]).toEqual({type: ADD_TOAST, text: "Все прекрасно"});
  });

  it('Test remove toast', async () => {
    const store = mockStore({toasts: []});
    await store.dispatch(removeToast(42315234523));
    expect(store.getActions()[0]).toEqual({type: REMOVE_TOAST, id: 42315234523});
  });
});

describe('Test store - loader', () => {

  it('Test show loader', async () => {
    const store = mockStore({loader: false});
    await store.dispatch(showLoader());
    expect(store.getActions()[0]).toEqual({type: SHOW_LOADER});
  });

  it('Test hide loader', async () => {
    const store = mockStore({loader: true});
    await store.dispatch(hideLoader());
    expect(store.getActions()[0]).toEqual({type: HIDE_LOADER});
  });
});

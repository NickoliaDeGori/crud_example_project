import React from 'react';
import PropTypes from 'prop-types';
import {mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Popup from '../src/app/components/Popup/Popup';
import createRouterContext from 'react-router-test-context'

configure({ adapter: new Adapter() });

describe('======> Test Popup Create contact', () => {
  let wrapper;
  Popup.contextTypes = {
    router: PropTypes.object
  };

  beforeEach(()=>{
    const context = createRouterContext();
    wrapper = mount(
        <Popup
            createContact={() => (Promise.resolve({error: null}))}
            isShow={true}
            toggleVisible={() => {}}
            goTo={() => {}}
            sendErrorAlert={() => {}}
            hideLoader={() => {}}
        />,
        {context}
    );
  });

  it('Should button is disabled after render', () => {
    expect(wrapper.state().isButtonDisabled).toBeTruthy();
  });

  it('Should state have name-error and button is disabled after enter "e" to first_name input', () => {
    wrapper.instance().onChangeText("first_name", {target: {value: "e"}});
    wrapper.update();
    expect(wrapper.find("#first_name_error").exists()).toBeTruthy();
    expect(wrapper.state().isButtonDisabled).toBeTruthy();
  });


  it('Should state have name-error and button is disabled after remove all words in email input', () => {
    wrapper.instance().onChangeText("first_name", {target: {value: ""}});
    wrapper.update();
    expect(wrapper.find("#first_name_error").exists()).toBeTruthy();
    expect(wrapper.state().isButtonDisabled).toBeTruthy();
  });

  it('Should state have phone-error and button is disabled after enter "1" to phone input', () => {
    wrapper.instance().onChangeText("phone", {target: {value: "1"}});
    wrapper.update();
    expect(wrapper.find("#phone_error").exists()).toBeTruthy();
    expect(wrapper.state().isButtonDisabled).toBeTruthy();
  });

  it('Should state have not name-error and button is disabled after enter "Cho" to name input', () => {
    wrapper.instance().onChangeText("first_name", {target: {value: "Cho"}});
    wrapper.update();
    expect(wrapper.find("#first_name_error").exists()).toBeFalsy();
    expect(wrapper.state().isButtonDisabled).toBeTruthy();
  });

  it('Should state have button is  not disabled after enter "+7912" to phone input and "Cho" to name input', () => {
    wrapper.instance().onChangeText("first_name", {target: {value: "Cho"}});
    wrapper.instance().onChangeText("phone", {target: {value: "+7912"}});
    wrapper.update();
    expect(wrapper.find("#phone_error").exists()).toBeFalsy();
    expect(wrapper.state().isButtonDisabled).toBeFalsy();
    wrapper.instance().onClickSave({preventDefault: ()=>{}});
  });
});

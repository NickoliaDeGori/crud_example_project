# React Form App

#### to start dev
```bash
npm start
```
#### to start build
```bash
npm run build
```
#### to test
```bash
npm run test
```

#### to test flow
```bash
npm run flow
```
